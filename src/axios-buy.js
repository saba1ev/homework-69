import axios from 'axios';

const snackBar = axios.create({
  baseURL: 'https://snackbar-sobolev.firebaseio.com/'
});

export default snackBar;