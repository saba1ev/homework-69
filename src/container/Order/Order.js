import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deleteBasket} from "../../store/actions";
import {NavLink} from "react-router-dom";

class Order extends Component {
  render() {
    let basket = [];
    for (let key in this.props.basket) {
      basket.push(this.props.basket[key].amount === 0 ? null : <div key={key}>
        <p>{this.props.basket[key].name}: X{this.props.basket[key].amount} Цена: {this.props.basket[key].price}</p>
        <button onClick={()=>this.props.deleteItem(key)}>Delete</button>
      </div>)
    }
    return (
      <div>
        {basket}
        {basket ?<NavLink to='/'>Go Back</NavLink> : null}
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return{
    basket: state.basket.basket
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    deleteItem: (name)=> dispatch(deleteBasket(name))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Order);