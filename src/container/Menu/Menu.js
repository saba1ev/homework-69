import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addToBasket, closeModal, fetchMenu, openModal} from "../../store/actions";
import './Menu.css'
import Modal from "../../components/UI/Modal/Modal";
import {NavLink} from "react-router-dom";
import Spinner from "../../components/UI/Spinner/Spinner";

class Menu extends Component {
  componentDidMount(){
    this.props.getMenuList()
  }
  render() {
    let menu = [];
    for (let key in this.props.menu){
      menu.push(<div key={key} className='menuList'>
        <img src={this.props.menu[key].url} alt=""/>
        <h3>{this.props.menu[key].name}</h3>
        <p>Цена: {this.props.menu[key].price} сом</p>
        <button onClick={() => this.props.addToBasket({name: this.props.menu[key].name, price: this.props.menu[key].price})}>Buy</button>
      </div>)
    }

    let basket = [];
    for (let key in this.props.basket) {
      basket.push(<div key={key}>
        <p>{this.props.basket[key].name}: X{this.props.basket[key].amount} Цена: {this.props.basket[key].price}</p>

      </div>)
    }

    return (
      <div>
        {this.props.loading? <Spinner/> :menu}
        <div className='boxPrice'>
          <p>Корзина</p>
          {basket}
          Общая цена: {this.props.totalPrice}
          {this.props.totalPrice !== 150 ? <button onClick={this.props.openModal}>Buy</button> : null}
        </div>
        <Modal show={this.props.open} close={this.props.closeModal}>
          <div>
            <p>Форма заказа</p>
            <form>
              <input type="number" placeholder='name'/>
              <input type="text" placeholder='name'/>
              <input type="email" placeholder='email'/>
            </form>
            <button><NavLink to='/order'>get Buy</NavLink></button>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return{
    menu: state.menu.menu,
    open: state.menu.open,
    loading: state.menu.loading,
    basket: state.basket.basket,
    totalPrice: state.basket.totalPrice
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    getMenuList: ()=> dispatch(fetchMenu()),
    openModal: ()=> dispatch(openModal()),
    closeModal: ()=> dispatch(closeModal()),
    addToBasket: (order)=> dispatch(addToBasket(order))
  }
}



export default connect(mapStateToProps, mapDispatchToProps) (Menu);