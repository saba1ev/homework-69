import {CLOSE_MODAL, FETCH_MENU_FAILURE, FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS, OPEN_MODAL,} from "./actions";

const initialState = {
  menu: null,
  loading: false,
  open: false,
};
const menuReducer = (state = initialState , action) =>{
  switch (action.type) {
    case FETCH_MENU_REQUEST:
      return{
        ...state,
        loading: true
      };
    case FETCH_MENU_SUCCESS:
      return{
        ...state,
        menu: action.data,
        loading: false
      };
    case FETCH_MENU_FAILURE:
      return{
        ...state,
        loading: false
      };
    case OPEN_MODAL:
      return{
        ...state,
        open: true
      };
    case CLOSE_MODAL:
      return{
        ...state,
        open: false
      };
    default:
      return state;
  }  

};

export default menuReducer;