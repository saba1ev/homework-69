import {ADD_TO_BASKET, CLOSE_MODAL, DELETE_BASKET} from "./actions";

const initialState = {
  basket: {},
  totalPrice: 150,

};

const basketReducer = (state = initialState, action) =>{
  switch (action.type) {
    case ADD_TO_BASKET:
      let order;
      if(state.basket[action.order.name]) {
        order = {...action.order, amount: state.basket[action.order.name].amount + 1};
      } else {
        order = {...action.order, amount: 1};
      }
      const basket = {...state.basket, [action.order.name]: order};
      return{
        ...state,
        basket: basket,
        totalPrice: state.totalPrice + action.order.price
      };
    case CLOSE_MODAL:
      return{
        ...state,
        basket: {},
        totalPrice: 150,
      };
    case DELETE_BASKET:
      let cart = {...state.basket};
      const newAmount = cart[action.name].amount - 1;
      cart[action.name].amount = newAmount;
      return{
        ...state,
        basket: cart
      }

    default: return state
  }
};

export default basketReducer;