import axios from '../axios-buy';

export const FETCH_MENU_REQUEST = 'MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'MENU_SUCCESS';
export const FETCH_MENU_FAILURE = 'MENU_FAILURE';


export const fetchMenuRequest = () =>{
  return ({type: FETCH_MENU_REQUEST});
};
export const fetchMenuSuccess = data =>{
  return ({type: FETCH_MENU_SUCCESS, data})
};
export const fetchMenuFailure = error =>{
  return({type: FETCH_MENU_FAILURE, error})
};


export const fetchMenu = () =>{
  return dispatch =>{
    dispatch(fetchMenuRequest());
    return axios.get('/menu.json').then(response=>{
      dispatch(fetchMenuSuccess(response.data))
    }, error =>{
      dispatch(fetchMenuFailure())
    })
  }
};

export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const DELETE_BASKET = 'DELETE_BASKET';
export const addToBasket = (order) =>{
  return {type: ADD_TO_BASKET, order};
};
export const deleteBasket = (name) =>{
  return{type: DELETE_BASKET, name}
}

export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const openModal = () =>{
  return{type: OPEN_MODAL}
};
export const closeModal = () =>{
  return{type: CLOSE_MODAL}
};

