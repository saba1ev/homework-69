import React, {Component} from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";
import Menu from "./container/Menu/Menu";
import Order from "./container/Order/Order";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={Menu}/>
        <Route path='/order' component={Order}/>
      </Switch>
    );
  }
}

export default App;
